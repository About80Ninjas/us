package org.henry.us.controller;

import java.util.Random;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
	
	@GetMapping("/")
	public String inde(Model model) {
		Random rand = new Random();
		int  n = rand.nextInt(50) + 1;
		model.addAttribute("trackingID", n);
		return "index";
	}

}
